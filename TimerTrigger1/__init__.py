from bs4 import BeautifulSoup
import datetime
import logging
import requests
import re
import azure.functions as func
import pandas as pd
import time


OSRS_URL = 'http://oldschool.runescape.com'
RS3_URL = 'http://www.runescape.com/player_count.js?varname=iPlayerCount&callback=jQuery000000000000000_0000000000&_=0'
OSRS_COUNT = 0
RS3_COUNT = 0
TOTAL_COUNT = OSRS_COUNT + RS3_COUNT

def osrs_count():
    r = requests.get(OSRS_URL).text
    soup = BeautifulSoup(r, "html5lib")
    OSRS_COUNT = int(re.search(r'\d+[,]\d+', soup.find('p', 'player-count').text).group().replace(',', ''))

def rs3_count():
    content = requests.get(RS3_URL, verify=False).text
    soup = BeautifulSoup(content, "html5lib")
    count = re.search(r'\((\d+)\)', soup.find('body').string).group()
    RS3_COUNT = int(re.search(r'\d+', count).group()) - osrs_count()
    
def get_counts():
    osrs_count()
    rs3_count()

def write_to_csv():
    df = pd.DataFrame(
        [
            {
                'timestamp': time.time(),
                'rs3': RS3_COUNT,
                'osrs': OSRS_COUNT,
                'total': TOTAL_COUNT
            }
        ]
    )
    return df.to_csv('population.csv', header=True, index=False, mode='a')

def main(mytimer: func.TimerRequest) -> None:
    get_counts()
    utc_timestamp = datetime.datetime.utcnow().replace(
        tzinfo=datetime.timezone.utc).isoformat()

    if mytimer.past_due:
        logging.info('The timer is past due!')

    logging.info('Python timer trigger function ran at %s', utc_timestamp)
    logging.info('Current RS3: %d', RS3_COUNT)
    logging.info('Current OSRS: %d', OSRS_COUNT)
    logging.info('Current Total: %d', TOTAL_COUNT)

    # outputBlob.set(write_to_csv())
